import sys
import random
import threading
from datetime import datetime


database = {}
record_quantity = int(sys.argv[1])
interactions = int(sys.argv[2])
average = 0

def populate_database():
    global database, record_quantity

    for i in range(0, record_quantity):
        database['RECORD_N_' + str(i)] = {
            'data': 'Lorem ipsum dolor sit amet, consectetur adipiscing elit...'
        }

def check():
    global average

    line = 'RECORD_N_{}'.format(str(random.randint(0, record_quantity - 1)))

    start = datetime.now()
    record = database.get(line)
    finish = datetime.now()

    average = average + (finish - start).microseconds

if __name__ == '__main__':
    populate_database()
    
    for i in range(0, interactions):
        execution = threading.Thread(target=check)
        execution.start()

    print('average = ', average / interactions)
